<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listed Cars - Car Inventory App</title>
    
    <link rel="stylesheet" href="../resources/css/foundation.css">
    <link rel="stylesheet" href="../resources/css/app.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	
</head>
<body>
	<!-- Navbar -->
	<div class="top-bar">
      <div class="top-bar-left">
        <ul class="menu">
          <li class="menu-text">Car Inventory App</li>
        </ul>
      </div>
      <div class="top-bar-right">
        <ul class="menu">
          <li><a href="/carinventoryapp/car/list">List</a></li>
          <li><a href="/carinventoryapp/car/add">Add</a></li>
        </ul>
      </div>
    </div>
 
	
	<div class="row column">
		<!-- Page Title -->
		<h3>Listed Cars</h3>
		
		<!--  List Table -->
		<table id="list-table" class="display">
		  <thead>
		    <tr>
		      <th width="200">Make</th>
		      <th width="200">Model</th>
		      <th width="150">Year</th>
		      <th width="150">Price</th>
		      <th width="150" id="controls"></th>
		    </tr>
		  </thead>
		  <tbody>
		  	<c:forEach items="${carList}" var="car">
		  		<tr>
		  			<td>${car.make}</td>
		  			<td>${car.model}</td>
		  			<td>${car.year}</td>
		  			<td>$${car.price}</td>
		  			<td width="80">
		  				<form:form action="list/delete" method="POST" modelAttribute="car"
		  						   cssStyle="float: left;">
		  					<input class="button small" type="submit" value="Delete"
		  						   onClick="return confirm('Are you sure you want to delete this car?')"/>
		  					<input type="hidden" name="carId" value="${car.id}"/>
		  				</form:form>
		  				<form:form action="list/edit" method="POST" modelAttribute="car"
		  						   cssStyle="float: right;">
		  					<input class="button small" type="submit" value="Edit"/>
		  					<input type="hidden" name="carId" value="${car.id}"/>
		  				</form:form>
         			</td>
		  		</tr>
	 		</c:forEach>
		  </tbody>
		</table>
	</div>
 	
 	<!--  Scripts -->
 	<script src="../resources/js/vendor/jquery.js"></script>
	<script src="../resources/js/vendor/foundation.min.js"></script>
	<script src="../resources/js/app.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
</body>
</html>