<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car Inventory App</title>
    
    <link rel="stylesheet" href="resources/css/foundation.css">
    <link rel="stylesheet" href="resources/css/app.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	
</head>
<body>
	<!-- Navbar -->
	<div class="top-bar">
    	<div class="top-bar-left">
        <ul class="menu">
          <li class="menu-text">Car Inventory App</li>
        </ul>
    	</div>
    </div>
    
    <br>
    
    <!-- Callout -->
    <div class="row">
      <div class="large-6 medium-6 columns">
        <div class="callout">
          <h5>View Car Inventory</h5>
        <p>See a sortable and searchable list of available cars.</p>
        <a href="car/list" class="small button">View List</a>
        </div>
      </div>
      <div class="large-6 medium-6 columns">
        <div class="callout">
          <h5>Add a Car</h5>
        <p>Fill out a form to add a car to the list of available cars.</p>
        <a href="car/add" class="small button">Add</a>
        </div>
      </div>
    </div>

	<!--  Scripts -->
 	<script src="resources/js/vendor/jquery.js"></script>
	<script src="resources/js/vendor/foundation.min.js"></script>
</body>
</html>