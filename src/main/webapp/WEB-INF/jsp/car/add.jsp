<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add a Car - Car Inventory App</title>
    
    <link rel="stylesheet" href="../resources/css/foundation.css">
    <link rel="stylesheet" href="../resources/css/app.css">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
</head>
<body>
	<!-- Navbar -->
	<div class="top-bar">
      <div class="top-bar-left">
        <ul class="menu">
          <li class="menu-text">Car Inventory App</li>
        </ul>
      </div>
      <div class="top-bar-right">
        <ul class="menu">
          <li><a href="/carinventoryapp/car/list">List</a></li>
          <li><a href="/carinventoryapp/car/add">Add</a></li>
        </ul>
      </div>
    </div>

	
	<!--  Form -->
	<div class="row column">
		<!--  Page Title -->
		<h3>Add a Car</h3>
		
		<!--  Form -->
			<form:form method="POST" modelAttribute="car">
				<p>
					Make:
					<input type="text" name="make" />
					<form:errors path="make" cssclass="error"></form:errors>
				</p>
				
				<p>
					Model:
					<input type="text" name="model" />
					<form:errors path="model" cssclass="error"></form:errors>
				</p>
				
				<p>
					Year:
					<input type="text" name="year" />
					<form:errors path="year" cssclass="error"></form:errors>
				</p>
				
				<p>
					Price:
					<input type="text" name="price" />
					<form:errors path="price" cssclass="error"></form:errors>
				</p>
				<input class="button" type="submit"/>
			</form:form>
	</div>
	
	<!--  Scripts -->
 	<script src="../resources/js/vendor/jquery.js"></script>
	<script src="../resources/js/vendor/foundation.min.js"></script>
	<script src="../resources/js/app.js"></script>
</body>
</html>



