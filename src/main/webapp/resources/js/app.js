// Init Foundation
$(document).foundation();


// Init jQuery DataTables with options
$(document).ready(function() {
    var table = $('#list-table').DataTable({
    	// Disable ordering for delete/edit column
    	"columns": [null, null, null, null, {"orderable": false, "targets": 0}],
    	
    	// Change page length values
    	"lengthMenu": [[5, 10, 15, 20, -1], [5, 10, 15, 20,"All"]]
    }); 
    
    // Change row color when selected
    $('#list-table tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
} );





