package com.patty.domain;

import java.math.BigDecimal;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class Car {
    @NotEmpty
    private String make;

    @NotEmpty
    private String model;

    @Min(1000)
    @Max(5000000)
    private BigDecimal price;

    private int id;

    @NotEmpty
    private String year;

    @Override
    public String toString() {
        return "Car [make=" + make + ", model=" + model + ", price=" + price + ", id=" + id + ", year=" + year + "]";
    }

    public String getMake() {
        return make;
    }

    public void setMake(String name) {
        this.make = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
