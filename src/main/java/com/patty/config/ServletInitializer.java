package com.patty.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/* This class replaces the standard web.xml file.
 * Tomcat will automatically call SpringServletContainerInitializer,
 * which will automatically detect this ServletInitializer.
 */
public class ServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[0];
    }

    // Declares the Spring config classes
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { AppConfig.class };
    }

    // Defines the servlet root uri
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }
}
