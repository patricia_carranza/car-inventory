package com.patty.dao;

import java.util.List;

import com.patty.domain.Car;

public interface CarDAO {
    public void add(Car car);

    public void edit(Car car);

    public void delete(int carId);

    public Car get(int carId);

    public List<Car> list();
}
