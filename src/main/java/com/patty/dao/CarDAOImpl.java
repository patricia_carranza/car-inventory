package com.patty.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.patty.domain.Car;

public class CarDAOImpl implements CarDAO {
    private final JdbcTemplate template;

    // dataSource is injected via constructor
    public CarDAOImpl(DataSource dataSource) {
        template = new JdbcTemplate(dataSource);
    }

    @Override
    public void add(Car car) {
        final String sql = "INSERT INTO car (make, model, price, year)" + "VALUES (?, ?, ?, ?)";
        template.update(sql, car.getMake(), car.getModel(), car.getPrice(), car.getYear());
    }

    @Override
    public void edit(Car car) {
        final String sql = "UPDATE car SET make=?, model=?, price=?, year=? WHERE car_id=?";
        template.update(sql, car.getMake(), car.getModel(), car.getPrice(), car.getYear(), car.getId());
    }

    @Override
    public void delete(int carId) {
        final String sql = "DELETE FROM car WHERE car_id=?";
        template.update(sql, carId);
    }

    @Override
    public List<Car> list() {
        final String sql = "SELECT * FROM car";
        final List<Car> listCars = template.query(sql, new RowMapper<Car>() {

            @Override
            public Car mapRow(ResultSet rs, int rowNum) throws SQLException {
                final Car car = new Car();

                car.setMake(rs.getString("make"));
                car.setModel(rs.getString("model"));
                car.setPrice(rs.getBigDecimal("price"));
                car.setYear(rs.getString("year"));
                car.setId(rs.getInt("car_id"));

                return car;
            }

        });

        return listCars;
    }

    @Override
    public Car get(int carId) {
        final String sql = "SELECT * FROM car where car_id=" + carId;
        return template.query(sql, new ResultSetExtractor<Car>() {

            @Override
            public Car extractData(ResultSet rs) throws SQLException, DataAccessException {
                if (rs.next()) {
                    final Car car = new Car();
                    car.setMake(rs.getString("make"));
                    car.setModel(rs.getString("model"));
                    car.setPrice(rs.getBigDecimal("price"));
                    car.setYear(rs.getString("year"));
                    car.setId(rs.getInt("car_id"));

                    return car;
                }
                return null;
            }
        });
    }

}
