package com.patty.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.patty.dao.CarDAO;
import com.patty.domain.Car;

@Controller
public class CarController {
    @Autowired
    private CarDAO carDAO;

    @RequestMapping("/")
    public String displayHomePage() {
        return "car/index";
    }

    @RequestMapping("/car/list")
    public void carList(Model model) {
        final List<Car> carList = carDAO.list();
        model.addAttribute("carList", carList);
    }

    @RequestMapping(value = "car/list/delete", method = RequestMethod.POST)
    public String carDelete(@RequestParam("carId") int carId) {
        carDAO.delete(carId);

        return "redirect:/car/list";
    }

    @RequestMapping(value = "car/list/edit-car", method = RequestMethod.POST)
    public String carEditSubmit(@ModelAttribute("car") @Valid Car car, BindingResult result) {
        if (result.hasErrors()) {
            return "car/edit";
        }

        carDAO.edit(car);
        return "redirect:/car/list";
    }

    @RequestMapping(value = "/car/list/edit", method = RequestMethod.POST)
    public ModelAndView carEdit(@RequestParam("carId") int carId) {
        final Car car = carDAO.get(carId);
        final ModelAndView model = new ModelAndView("car/edit");
        model.addObject("car", car);

        return model;
    }

    @RequestMapping("/car/add")
    public void carAdd() {
        // The JSP view resolver will implicitly display
        // /WEB-INF/jsp/car/add.jsp
    }

    @RequestMapping(value = "/car/add", method = RequestMethod.POST)
    public String carAddSubmit(@ModelAttribute("car") @Valid Car car, BindingResult result) {
        if (result.hasErrors()) {
            return "car/add";
        }

        carDAO.add(car);
        return "redirect:/car/list";
    }
}